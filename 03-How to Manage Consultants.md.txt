如何管理顾问
利用顾问，但是不要依赖他们。他们是极好的并且渴求尊敬。因为他们看了很多不同的项目，他们通常比你知道更多关于明确的技术，甚至是编程技术。使用他们的最好方法是作为通过举例讲课的机构内部的教育者。
然而，要是因为你没有足够的时间学习他们的优点和缺点，在某种意义上，他们通常不能成为普通员工组成的团队的一部分，。他们的财政承诺更低。他们可以更加容易低换地方。如果公司做的好，他们可能拥有较少的收益。一些是好的，一些是平均值，一些是坏的，但是希望你选择的顾问没有你选择的员工仔细，这样你就会得到一些更坏的。
如果你的顾问打算写代码，你必须仔细地复查它。你不能使一个项目在结束的时候带有一大块没有复查出来的代码的风险。事实上，这是所有团队成员的真实，但是你将会有更多团队成员的知识靠近你。